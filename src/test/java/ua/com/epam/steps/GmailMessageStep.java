package ua.com.epam.steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import ua.com.epam.businessobject.GmailHomeBO;
import ua.com.epam.businessobject.GmailLoginBO;
import ua.com.epam.businessobject.SentMessageBO;
import ua.com.epam.utils.DataPropGmail;

import java.util.Random;

import static org.testng.Assert.assertTrue;
import static ua.com.epam.webdriver.LocalDriverManager.closeDriver;
import static ua.com.epam.webdriver.LocalDriverManager.getWebDriver;

public class GmailMessageStep {
    protected DataPropGmail dataProp;
    private String randomMsg = getRandomNumber();

    @Before
    public void setup() {
        dataProp = new DataPropGmail();
        getWebDriver().get(dataProp.getGmailPage());
    }

    @When("^open gmail and login using login as \"(.*)\" and password as \"(.*)\"$")
    public void login(String login, String password) {
        GmailLoginBO loginPage = new GmailLoginBO();

        loginPage.loginToGmail(login, password);
    }

    @And("^click 'Compose' button$")
    public void clickCompose() {

        new GmailHomeBO().clickCompose();
    }

    @And("^fill 'To' as \"(.*)\", 'Subject' as \"(.*)\" and 'message' fields and click 'sent' button$")
    public void setMessage(String recipient, String topic) {

        new GmailHomeBO().writeAndSendMessage(
                recipient,
                topic,
                randomMsg);
    }

    @Then("^click 'sent' tab$")
    public void clickSentTab() {
        new GmailHomeBO().navigateToSentLetter();
    }

    @Then("^verify that message is in 'sent' folder$")
    public void verifyMessage() {
        assertTrue(new SentMessageBO().getMessageText().contains(randomMsg)
                , String.format("Letter with message - '%s' wasn't sent.", randomMsg));

    }

    @Then("^remove message from the 'sent' folder$")
    public void removeMessage() {
        new SentMessageBO().deleteFirstLetter();
    }

    protected String getRandomNumber() {
        Random rd = new Random();
        return String.valueOf(rd.nextInt((9999999 - 1) + 1) + 1);
    }

    @After
    public void closeSite() {
        closeDriver();
    }
}
