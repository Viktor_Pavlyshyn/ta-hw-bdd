package ua.com.epam.runner;


import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import ua.com.epam.listener.AllureListener;

@Listeners(AllureListener.class)
@CucumberOptions(features = {"src/test/resources/gmailMessage.feature"},
        glue = {"ua/com/epam/steps"})
public class GmailMessageTest extends AbstractTestNGCucumberTests {

    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }
}