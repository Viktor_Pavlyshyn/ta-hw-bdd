Feature: Login gmail, sent message, check is this message send and delete all messages

  Scenario Outline: Login gmail, sent message, check is this message send and delete all messages
    When open gmail and login using login as "<Mail>" and password as "<Password>"
    Then click 'Compose' button
    And fill 'To' as "<Recipient>", 'Subject' as "<Topic>" and 'message' fields and click 'sent' button
    And click 'sent' tab
    Then verify that message is in 'sent' folder
    Then remove message from the 'sent' folder

    Examples:
      | Mail                 | Password  | Recipient         | Topic         |
      | test1selen@gmail.com | root1test | lucean7@gmail.com | selenium test |
      | test2selen@gmail.com | root2test | lucean7@gmail.com | selenium test |
      | test5selen@gmail.com | root5test | lucean7@gmail.com | selenium test |
      | test6selen@gmail.com | root6test | lucean7@gmail.com | selenium test |
      | test7selen@gmail.com | root7test | lucean7@gmail.com | selenium test |