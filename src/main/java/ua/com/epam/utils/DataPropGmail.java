package ua.com.epam.utils;


import static ua.com.epam.config.GmailProperties.GMAIL_LOGIN_PAGE;

public class DataPropGmail extends PropertiesReaderDB {

    public String getGmailPage() {
        return props.getProperty(GMAIL_LOGIN_PAGE);
    }

}
