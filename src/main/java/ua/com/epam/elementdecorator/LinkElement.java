package ua.com.epam.elementdecorator;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LinkElement extends AbstractElement {

    public LinkElement(WebElement element) {
        super(element);
    }

    public LinkElement getTextAfterDocumentReadyState() {
        fwait.until((ExpectedCondition<Boolean>) wd -> ((JavascriptExecutor) wd)
                .executeScript("return document.readyState").equals("complete"));
        element.getText();
        return this;
    }

    public LinkElement clickAfterVisibilityOf() {
        fwait.until(ExpectedConditions.visibilityOf(element));
        element.click();
        return this;
    }
}
