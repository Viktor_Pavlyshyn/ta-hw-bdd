package ua.com.epam.businessobject;

import lombok.extern.log4j.Log4j2;
import ua.com.epam.pageobject.GmailHomePage;

@Log4j2
public class GmailHomeBO {

    private final GmailHomePage gmailHomePage;

    public GmailHomeBO() {
        this.gmailHomePage = new GmailHomePage();
    }

    public GmailHomeBO clickCompose(){

        log.info("Click by button 'Write'.");
        gmailHomePage.getComposeButton().clickAfterDocumentReadyState();

        return this;
    }

    public GmailHomeBO writeAndSendMessage(String recipient, String topic, String textMessage) {

        log.info("Setting recipient - {}.", recipient);
        gmailHomePage.getRecipientArea()
                .setTextAfterPresenceOfAllElementsLocatedBy(recipient, gmailHomePage.getRecipientAreaXpath());

        log.info("Setting topic - {}.", topic);
        gmailHomePage.getTopicArea().cleanBeforeSendKeys(topic);

        log.info("Setting message - {}.", textMessage);
        gmailHomePage.getMessageArea().cleanBeforeSendKeys(textMessage);

        log.info("Sending letter.");
        gmailHomePage.getSendButton().getWrappedElement().click();

        return this;
    }

    public SentMessageBO navigateToSentLetter() {
        log.info("Navigate to 'sent letter' tab.");
        gmailHomePage.getSentMessageTab()
                .clickAfterStalenessOfElement(gmailHomePage.getSentMsgTabXpath());

        return new SentMessageBO();
    }
}
